module codeberg.org/u1f320/pronouns.cc

go 1.18

require (
	emperror.dev/errors v0.8.1
	github.com/Masterminds/squirrel v1.5.2
	github.com/bwmarrin/discordgo v0.25.0
	github.com/georgysavva/scany v0.3.0
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-chi/httprate v0.5.3
	github.com/go-chi/render v1.0.1
	github.com/golang-jwt/jwt/v4 v4.4.1
	github.com/jackc/pgconn v1.12.0
	github.com/jackc/pgx/v4 v4.16.0
	github.com/joho/godotenv v1.4.0
	github.com/mediocregopher/radix/v4 v4.1.0
	github.com/rs/xid v1.4.0
	github.com/rubenv/sql-migrate v1.1.1
	go.uber.org/zap v1.21.0
	golang.org/x/oauth2 v0.0.0-20210402161424-2e8d93401602
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/go-gorp/gorp/v3 v3.0.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.11.0 // indirect
	github.com/jackc/puddle v1.2.1 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/klauspost/cpuid/v2 v2.1.0 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/minio/md5-simd v1.1.2 // indirect
	github.com/minio/minio-go/v7 v7.0.37 // indirect
	github.com/minio/sha256-simd v1.0.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	github.com/tilinna/clock v1.0.2 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa // indirect
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
	gopkg.in/ini.v1 v1.66.6 // indirect
)
